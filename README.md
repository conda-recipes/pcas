# pcas conda recipe

Home: https://github.com/epics-modules/pcas

Package license: EPICS Open License

Recipe license: BSD 2-Clause

Summary: C++ Portable Channel Access Server Library
