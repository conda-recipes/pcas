#!/bin/bash

cat << EOF > configure/CONFIG_SITE.local
INSTALL_LOCATION=${EPICS_BASE}
EOF

cat << EOF > configure/RELEASE.local
EPICS_BASE=$EPICS_BASE
EOF

make
